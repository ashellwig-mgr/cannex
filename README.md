# CannExchange

The premier open sourced, subscription based Cannabis and Hemp Exchange

## Installation

To install, ensure that you have all of the dependencies first:

* External Packages:
  * [Yarn](https://yarnpkg.com/lang/en/docs/install/#arch-stable)

* Node Packages:
  * `yarn global add firebase-tools @vue/cli`

## Configuration

### Environment Variables

Make sure that you have created a `.env` file in the root of the project
  with the following information:

```bash
# Firebase Authentication
API_KEY=
AUTH_DOMAIN=
DATABASE_URL=
PROJECT_ID=
STORAGE_BUCKET=
MESSAGING_SENDER_ID=
```

You can find this at

`https://console.firebase.google.com/u/0/project/<PROJECT_ID>/authentication/users`

then select **Web Setup** in the top right corner. Make sure to replace
`PROJECT_ID` with your project ID.

### Vue Configuration

Vue can be conigured via *vue.config.js* but the following is **REQUIRED**
  for the environment variables to be present in the `dev` server.

```javascript
// vue.config.js
const Dotenv = require('dotenv-webpack')
const dotenv = require('dotenv')

dotenv.config()

module.exports = {
  configureWebpack: {
    plugins: [
      new Dotenv()
    ]
  }
}

```