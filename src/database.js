import Vue from 'vue'
import firebase from 'firebase/app'
import VueFire from 'vuefire'
import '@firebase/firestore'
import '@firebase/auth'
import '@firebase/storage'
import VueFireStore from 'vue-firestore'

Vue.use(VueFire)
Vue.use(VueFireStore)

var config = {
  apiKey: process.env.API_KEY,
  authDomain: process.env.AUTH_DOMAIN,
  databaseURL: process.env.DATABASE_URL,
  projectId: process.env.PROJECT_ID,
  storageBucket: process.env.STORAGE_BUCKET,
  messagingSenderId: process.env.MESSAGING_SENDER_ID
}

export default firebase.initializeApp(config)

export const settings = {
  timestampsInSnapshots: true
}

export const auth = firebase.auth()
export const storage = firebase.storage()
export const db = firebase.firestore()

db.settings(settings)
