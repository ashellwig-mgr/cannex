import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './database'
import './registerServiceWorker'
import firebase from 'firebase/app'
import { config } from 'dotenv'

config()

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)

  if (requiresAuth && !currentUser) next('/welcome')
  else if (requiresAuth && currentUser) next()
  else next()
})

let app

firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    app =
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
