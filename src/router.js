import Vue from 'vue'
import 'firebase/auth'
import Router from 'vue-router'
import Home from './views/Home.vue'
import News from './views/News.vue'
import About from './views/About.vue'
import Blog from './views/Blog.vue'
import Education from './views/Education.vue'
import Classifieds from './views/Classifieds.vue'
import Verify from './views/classifieds/Verify.vue'
import Wanted from './views/classifieds/Wanted.vue'
import Available from './views/classifieds/Available.vue'
import Login from './views/Login.vue'
import Registration from './views/Registration.vue'
import Welcome from './views/Welcome.vue'
import Shop from './views/Shop.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/home'
    },
    {
      path: '/welcome',
      name: 'welcome',
      component: Welcome
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/news',
      name: 'news',
      component: News,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/about/blog',
      name: 'blog',
      component: Blog,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/education',
      name: 'education',
      component: Education,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/Classifieds',
      name: 'Classifieds',
      component: Classifieds,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/shop',
      name: 'shop',
      component: Shop,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'registration',
      component: Registration
    },
    {
      path: '/classifieds/wanted',
      name: 'wanted',
      component: Wanted,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/classifieds/available',
      name: 'available',
      component: Available,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/classifieds/verify',
      name: 'verify',
      component: Verify,
      meta: {
        requiresAuth: true
      }
    }
  ]
})
