import { shallowMount } from '@vue/test-utils'
import { db } from '../../src/database'
import Blog from '../../src/views/Blog.vue'
import Vue from 'vue'
import Vuetify from 'vuetify'

Vue.use(Vuetify)

describe('Blog View (src/views/Blog.vue)', () => {
  it('renders data from firestore', () => {
    const wrapper = shallowMount(Blog, {})
    expect(wrapper.posts === db.collection('posts').posts)
    db.app.delete()
  })
})
