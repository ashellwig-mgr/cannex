const Dotenv = require('dotenv-webpack')
const dotenv = require('dotenv')

dotenv.config()

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    plugins: [
      new Dotenv()
    ]
  }
}
